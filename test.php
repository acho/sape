<?php

require 'xml/lib/xmlrpc.inc'; // required this library
require 'SapeApi.class.php';

$user = '';
$pass = '';

$sape = new SapeApi();

$sape->set_debug(0)->connect($user, $pass);

$get_user = $sape->query('sape.get_user')->fetch();

$projects = $sape->query('sape.get_projects')->fetch();